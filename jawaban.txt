1.Membuat database
creat database myshop
2.Membuat table
 #.Table categories =>
    create table categories(
    -> id int auto_increment primary key,
    -> name varchar(255)
    -> );
 #.Table items =>
    create table items(
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id)references categories(id)
    -> );
 #.Table users =>
    create table users(
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
3.Memasukkan data pada table
 #.Table categories =>
insert into categories(name) values('gadget'),('cloth'),('men'),('women'),('branded');